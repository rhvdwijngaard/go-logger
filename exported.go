package logger

import (
	"github.com/Sirupsen/logrus"
)

// This whole file is basically copied from logrus, to allow us to not depend on logrus directly.
var (
	// std is the name of the standard logger in stdlib `log`
	std = CreateLogger()
)

func panicIfNotConfigured() {
	if std.mandatory == nil || std.logger == nil {
		panic("The go-logger has not been configured. Please initialize it with mandatory fields.")
	}
}

func StandardLogger() *logrus.Entry {
	return std.logger
}

// SetLevel sets the standard logger level.
func SetLevel(level Level) {
	l, err := logrus.ParseLevel(level.String())

	if err != nil {
		std.logger.Fatal(err)
	}

	std.level = level
	logrus.SetLevel(l)
}

func GetLevel() Level {
	return std.level
}

func WithField(key string, value interface{}) *logrus.Entry {
	return std.logger.WithField(key, value)
}

/*
func WithFields(fields logrus.Fields) *logrus.Entry {
	return std.logger.WithFields(fields)
}*/

func Debug(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Debug(args...)
}

func Print(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Print(args...)
}

func Info(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Info(args...)
}

func Warn(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Warn(args...)
}

func Warning(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Warning(args...)
}

func Error(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Error(args...)
}

func Panic(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Panic(args...)
}

func Fatal(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Fatal(args...)
}

func Debugf(format string, args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Debugf(format, args...)
}

func Printf(format string, args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Printf(format, args...)
}

func Infof(format string, args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Infof(format, args...)
}

func Warnf(format string, args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Warnf(format, args...)
}

func Warningf(format string, args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Warningf(format, args...)
}

func Errorf(format string, args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Errorf(format, args...)
}

func Panicf(format string, args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Panicf(format, args...)
}

func Fatalf(format string, args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Fatalf(format, args...)
}

func Debugln(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Debugln(args...)
}

func Println(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Println(args...)
}

func Infoln(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Infoln(args...)
}

func Warnln(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Warnln(args...)
}

func Warningln(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Warningln(args...)
}

func Errorln(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Errorln(args...)
}

func Panicln(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Panicln(args...)
}

func Fatalln(args ...interface{}) {
	panicIfNotConfigured()
	std.logger.Fatalln(args...)
}
