package logger

import (
	log "github.com/Sirupsen/logrus"
	"net"
	"errors"
)

type MandatoryLogFields struct {
	ServiceName string
	InvokedBy string
	IP net.IP
	Identification string
	CorrelationId string
}

type Logger struct {
	mandatory *MandatoryLogFields
	logger *log.Entry
	level Level
}

func verify(fields *MandatoryLogFields) (error) {
	if fields == nil {
		return errors.New("go-logger: mandatory fields struct cannot be nil.")
	}
	if fields.ServiceName == "" {
		return errors.New("go-logger: ServiceName must be set.")
	}
	if fields.InvokedBy == "" {
		return errors.New("go-logger: InvokedBy must be set.")
	}
	if fields.IP == nil {
		return errors.New("go-logger: IP must be set.")
	}
	if fields.Identification == "" {
		return errors.New("go-logger: Identification must be set.")
	}
	if fields.CorrelationId == "" {
		return errors.New("go-logger: CorrelationId must be set.")
	}
	return nil
}

func CreateLogger() *Logger {
	logger := &Logger{
		mandatory: nil,
		logger: nil,
		level: InfoLevel,
	}
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)

	return logger
}

func Initialize(mandatoryFields *MandatoryLogFields) error {
	err := verify(mandatoryFields)

	if err != nil {
		return err
	}

	std.mandatory = mandatoryFields

	std.logger = log.WithFields(log.Fields{
		"ServiceName": mandatoryFields.ServiceName,
		"InvokedBy": mandatoryFields.InvokedBy,
		"IP": mandatoryFields.IP,
		"Identification": mandatoryFields.Identification,
		"CorrelationId": mandatoryFields.CorrelationId,
	})

	return nil
}
