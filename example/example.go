package main

import (
	log "bitbucket.org/to-increase/go-logger"
	"net"
)

func main() {
	// Make sure that all required fields are provided. Every member of this struct is mandatory.
	mandatory := &log.MandatoryLogFields{
		ServiceName: "MyService",
		InvokedBy: "MyInvoker",
		IP: net.ParseIP("127.0.0.1"),
		Identification: "MyIdentification",
		CorrelationId: "MyCorrelationId",
	}

	// Initialize the logger. You only have to do this once, on startup.
	err := log.Initialize(mandatory)

	// If an error is returned the logger cannot be instantiated. This is a case in which it is ok to panic.
	if err != nil {
		panic(err)
	}

	// In the rest of the application you can now just import the go-logger as 'log', and use that as log reference.
	log.Info("This will be logged")

	log.Debug("This will not, since the default level is info.", 1, 2, "a third extra var")

	log.SetLevel(log.DebugLevel)

	log.Debug("Logged because of increased log level.")

	log.WithField("MyAnnotation", "You can add annotations as well").Info("My message")

	log.SetLevel(log.InfoLevel)

	extra := log.WithField("reuse", "Annotations can also be reused. A logger can be 'enriched'.")

	extra.Info("My info annotated log message.")
	extra.Debug("My debug annotated log message.")

	//extra.Panic("We can even panic with the annotated logger.")
	extra.Info("... but let's not do that.")
}